# 1. Czym Jest programowanie? Algorytmy
Program komputerowy to szereg instrukcji wpisanych w kolejnych liniach. Komputer wykonuje te instrukcje kolejno.

Tutaj można napisać najprostszy program składający się z kilku instrukcji print wyświetlających różne słowa – aby zilustrować dzieciom, że komputer wykonuje instrukcje kolejno, z góry na dół.
```python
print('witaj \n')
print('na lekcji programowania \n')
print('trzecia linijka tekstu \n')
```
\n – służy do ‘zrobienia’ nowej linijki, można ten element z dziećmi usunąć i uruchomić taki program:
```python
print('witaj ')
print('na lekcji programowania ')
print('trzecia linijka tekstu ')
```
wszystkie instrukcje zostaną wykonane kolejno ale przez brak \n napisy wyświetlą się w jednej linii.

Algorytm jest rodzajem instrukcji uwzględniającej podejmowanie decyzji. Warto opisać dzieciom przykład algorytmu, np. algorytm kupowania loda:
```
- Czy mam ochotę na loda?
	Jeśli nie → koniec algorytmu
	Jeśli tak → przejdź do kolejnego kroku
- Idź do sklepu.
- Sprawdź, czy w sklepie są lody?
	Jeśli nie → koniec algorytmu
	Jeśli tak:
		czy są lody, na które mam ochotę?
					Jeśli nie → koniec algorytmu
					Jeśli tak:
						czy mam odpowiednią ilość pieniędzy?
							Jeśli nie → koniec algorytmu
							Jeśli tak:
								odlicz kwotę
								kup lody
								koniec algorytmu
```
Algorytm może być ogólny lub szczegółowy – zależnie od potrzeb.
Warto poprosić dzieci aby spróbowały dodać jakiś element do takiego algorytmu (co jeszcze można po drodze sprawdzić? Jakie pytania zadać?)

***Ważne: algorytm nie powinien zawierać rzeczy zbędnych. Owszem dodanie kroku polegającego na zapytaniu rodziców o pozwolenie na zakupy jest dobrym pomysłem, ale jeżeli dziecko zacznie się rozwodzić nad dobraniem ubrania do wyjścia do sklepu – będzie brnąć w niepotrzebne elementy. Należy podkreślić, że algorytm powinien zawierać tylko niezbędne elementy. Algorytm można uszczegółowić ale nie należy go niepotrzebnie komplikować.***
Dzieci powinny dostać zadanie polegające na przedstawieniu własnych przykładów prostych algorytmów i/lub opisaniu wskazanego algorytmu wskazanego przez Ciebie prostej czynności.

# 2. Typy danych i zmienne.
Na początku lekcji warto krótko przypomnieć zagadnienie algorytmu i poprosić dzieci o podanie przykładów algorytmów.

Aby komputer mógł pracować z informacjami, które mu podamy – musi rozpoznać typ wprowadzonej informacji. W Pythonie jest wiele typów danych ale możecie (a może nawet powinniście) ograniczyć się do:

Tekstu: ***str*** (od angielskiego słowa „string” – łańcuch, w znaczeniu ciąg znaków wpisanych z klawiatury)
Liczb całkowitych: ***int*** (od integer)
Liczb zmiennoprzecinkowych, czyli ułamków dziesiętnych: ***float***
Wartości logicznej prawda/fałsz: ***bool*** (od nazwiska angielskiego matematyka George’a Boole’a)

Najważniejsze w programowaniu są zmienne logiczne. Procesor komputera rozpoznaje tylko dwa stany – prawdę reprezentowana jako 1 i fałsz, reprezentowany 0. Dla procesora 1 jest wtedy gdy dociera do niego odpowiednio wysokie napięcie elektryczne, zero zaś gdy napięcia nie ma lub jest zbyt niskie.
W programowaniu 0 logiczne można zastąpić słowem false a logiczną 1 słowem true. To zostanie dokładniej wyjaśnione na lekcji o blokach decyzyjnych. ***Wystarczy wiedza iż 0 to fałsz (po angielsku false) a 1 to prawda (po angielsku true) i określanie prawdy lub fałszu umozliwi nam sterowanie programem...***

Ważne jest to aby zwrocic uwagę na odróżnianie typów danych:

Jeżeli komputer spodziewa się danych typu str (string – tekst) to nawet jeżeli wpiszemy liczby to będą one rozpoznane jako tekst – nie będzie możliwe wykonanie na nich działań matematycznych. Podobnie np. słowo false, wpisane gdy komputer spodziewa się tekstu, jest tylko ciągiem znaków. Ciekawostka – każda wartość int wyższa od zera podana gdy komputer spodziewa się wartości bool zostanie zinterpretowana jako logiczna jedynka, dowolne słowo str podane gdy komputer spodziewa się wartości bool, będzie zinterpretowane jako logiczna jedynka, nawet słowo false, gdy jest podane jako string zwraca wartość 1. Ale o tym na następnej lekcji.

# 3. Pobieranie danych z klawiatury, zmienne

W programowaniu zajmujemy się przechowywaniem informacji i działaniem na informacjach. Służą do tego ***zmienne***. Zmienne są jak szufladki - możemy w nich umieścić różne rzeczy po to by je tam przechowywać i sięgnąć po nie kiedy jest taka potrzeba. te nasze 'szufladki', czyli zmienne muszą mieć swoje nazwy, każda zmienna może przechowywać informacje tylko z jednego typu danych. Jeżeli ustaliliśmy zmienną jako typ ***int*** to komputer nie pozwoli nam umieśić w tej zmiennej liter. Jeżeli zaś ustaliliśmy zmienną na typ ***str*** to każada wpisana do niej liczba zostanie uznana za tekst.

Napiszmy pierwszy program, który ma przywitać się z uzytkownikiem.

Algorytm wygląda tak:

```
- Wyświetl pytanie o imię.
- Pozwól uzytkownikowi wpisać imię za pomocą klawiatury.
- wyświetl powitanie oraz imie użytkownika.
```

Program wygląda tak:

```python
print ('Cześć. Jak masz na imię? \n')
imie = input()
print('Witaj '+ imie +' Miło Cię poznać!')
```

Zrealizowaliśmy nasz algorytm.
W pierwszej linni wyświetliliśmy pytanie. W drugiej pozwolilismy uzytkownikowi wpisać tekst i przypisalismy tekst do zmiennej *imie*, w trzeciej linijce wyświetliliśmy powitanie oraz zawartość zmiennej *imie*.

Zwróćmy uwagę:

Do wyświetlania informacji słuzy instrukcja **print**, po tej instrukcji, w nawiasach, wpisujemy to co chcemy wyświetlić.

Tekst ujmujemy zawsze w cudzysłów - nie ma znaczenia pojedynczy czy podwójny.

Nazwa zmiennej, którą chcemy wyświetlic nie moze znajdować się w cudzysłowie, jest to bowiem zmienna zawierająca tekst a nie sam tekst (szufladka z tekstem nie jest tym samym co tekst).

Do łączenia szufladek, czyli zmiennych, z tekstem służą plusiki.

# 4. Liczby, instrukcje warunkowe, wcięcia i operatory porównania

Sprawdźmy, czy użytkownik jest w wmieku szkolnym.

Do sprawdzania wartości i sterowania programem słuzy instrukcja ***if*** (ang. jeżeli)

Algorytm:

```
- zapytaj uzytkownika ile ma lat
- pozwól uzytkownikowi wpisać informację z klawiatury
- jeżeli wpisana liczba jest mniejsza niż sześć, wyświetl informację, iz nie jest to wiek szkolny
- w przeciwnym wypadku wyświetl informacje, iż jest to wiek szkolny
```

Program:
```python
print('Cześć. Ile masz lat?')
wiek = input()
if int(wiek) < 6:
    print('Niestety, jeszcze za wcześnie by iść do szkoły!')
else:
    print('Uff! Już możesz się uczyć!')
```
Pierwsze dwie linijki są już nam znane - witamy się z uzytkownikiem i wczytujemy dane z klawiatury do zmiennej *wiek*.
W trzeciej linijce stosujemy instrukcję ***if*** aby sprawdzić czy zawartość zmiennej *wiek* jest mniejsza niż sześć.
Zwróćmy uwagę, że odwołujemy się do zmiennej wiek nie tak jak poprzednio, przez samą tylko nazwę, tym razem stosujemy formę ***int(wiek)*** - wywołujemy funkcję ***int()***, w której nawiasach podajemy nazwę zmiennej. Funkcja ***int()*** zmusza komputer by zawartość zmiennej *wiek* rozumiał jako liczbę *int*.

Po słowie ***if*** podajemy warunek, którego prawdziwość chcemy sprawdzić, w tym wypadku ***if int(wiek) < 6:*** rozumiemy jako ***jeżeli [liczba znajdująca się w zmiennej wiek] jestmniejsza niż 6***. Po warunku umieszczamy dwukropek. Następnie zaczyna się lista instrukcji, które będą wykonane ***jeżeli warunek, podany w instrukcji if, jest prawdziwy***. każda linijka w tej liście instrukcji musi się zaczynać od wcięcia wykonanego klawiszem [TAB].
Po instrukcji ***if*** może opcjonalnie występowac instrukcja ***else*** (ang. "w przeciwnym wypadku"), po której równiez umieszczamy dwukropek, instrukcja ***else*** rozpoczyna blok poleceń, które zostana wykonane jedynie wtedy gdy warunek określony w ***if*** okaże się ***nieprawdziwy***.
Istnieje także instrukcja ***elif*** - umozliwia określenie nowego warunku logicznego, który zostanie ***sprawdzony jedynie wtedy gdy warunek określony w if okaże się nieprawdziwy***. Ale to można wprowadzic na późniejszych lekcjach.
W tym momencie ważne są operatory porównania:
```
== - jest równe
!= - nie równa się
> - większe niż
< - mniejsze niż
>= - większe lub równe
<= - mniejsze lub równe
```

# 5. Zaawansowane instrukcje warunkowe i operatory logiczne

Udoskonalmy nasz program sprawdzający czy uzytkownik jest w wieku szkolnym. Postarajmy sie określić, na jakim etapie edukacji może być. Oczywiście przy idealnym założeniu, że uzytkownik uczy sie w miarę dobrze i nie powtarza klasy.

Algorytm:
```
- zapytaj uzytkownika ile ma lat
- pozwól uzytkownikowi wpisać informację z klawiatury
- jeżeli wpisana liczba jest większa lub równa sześć i mniejsza lub równa 15, wyświetl informację, iż uzytkownik jest w szkole podstawowej
- w przeciwnym wypadku, gdy wiek jest niższy niż 20, wyświetl informację iż uzytkownik jest uczniem liceum lub technikum
- w przeciwnym wypadku, gdy wiek jest wyższy niz 20, wyświetl informacje, że uzytkownik jest studentem lub osobą już nie uczącą się.
- w przeciwnym wypadku (ten dzieje się tylko wtedy gdy wiek bedzie niższy niż 6), wyświetl informacje iż uzytkownik nie jest w wkieku edukacyjnym
```

Program:


```python
print('Cześć. Ile masz lat?')
wiek = input()
if int(wiek) >= 6 and int(wiek) <=15:
    print('Gratulacje. Uczysz się w szkole podstawowej!')
elif int(wiek) > 6 and int(wiek) <= 19:
    print('Gratulacje. Uczysz się w szkole średniej.')
elif int(wiek) > 19:
    print('Gratulacje. Studiujesz lub udało ci się skończyć edukację.')
else:
    print('Niestety, jeszcze za wcześnie by iść do szkoły!')
```

W dwóch przypadkach zastosowaliśmy operator logiczny ***and***, który powoduje, że warunek podany w instrukcjach ***if, elif*** jest prawdziwy wtedy i tylko wtedy prawdziwe są oba warunki - ten podany przed operatorem ***and*** i ten podany po nim. W trzeciej linijce widzimy warunek: ***if int(wiek) >= 6 and int(wiek) <=15:***, komputer sprawdzi więc czy liczba podana w zmiennej wiek jest wieksza lub równa 6, sprawdzi też czy liczba podana w zmiennej wiek jest mniejsza lub równa 15 i tylko gdy te dwa proste warunki bedą prawdziwe, całość wyrażenia po ***if*** zostanie uznana za prawdę.

W Pythonie mamy do dyspozycji trzy operatory logiczne:

```
and - zwraca prawdę gdy elementy po obu stronach są prawdziwe
or - zwraca prawdę gdy conalmniej jeden element, po którejkolwiek ze stron, jest prawdziwy
not - jest logicznym zaprzeczeniem, zwraca wartość odwrotną do ustalonej
```

# 6. Pętla While i prosta gra
todo
# 7. Pętla For
todo
# 8. Listy i tablice
todo
# 9. Funkcje i instrukcja return
todo
# 10.Opcjonalnie: RegExp i import
todo
# 11. Opcjonalnie: grafika i pyGame