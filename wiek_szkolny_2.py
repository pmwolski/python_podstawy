print('Cześć. Ile masz lat?')
wiek = input()
if int(wiek) >= 6 and int(wiek) <=15:
    print('Gratulacje. Uczysz się w szkole podstawowej!')
elif int(wiek) > 6 and int(wiek) <= 19:
    print('Gratulacje. Uczysz się w szkole średniej.')
elif int(wiek) > 19:
    print('Gratulacje. Studiujesz lub udało ci się skończyć edukację.')
else:
    print('Niestety, jeszcze za wcześnie by iść do szkoły!')